import 'dart:async';

import 'package:devtools_app_shared/ui.dart';
import 'package:devtools_extensions/devtools_extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fancy_tree_view/flutter_fancy_tree_view.dart';
import 'package:proto/tree_node.dart';
import 'package:vm_service/vm_service.dart';

void main() {
  // DevToolsExtension must be at the root to perform important initializations
  runApp(const DevToolsExtension(child: ProtoDevToolsExtension()));
}

class ProtoDevToolsExtension extends StatefulWidget {
  const ProtoDevToolsExtension({super.key});

  @override
  State<ProtoDevToolsExtension> createState() => _ProtoDevToolsExtensionState();
}

class _ProtoDevToolsExtensionState extends State<ProtoDevToolsExtension> {
  StreamSubscription<Event>? clientEventSub;
  var _rootCount = 0;
  var _roots = <TreeNode>[];

  late final TreeController<TreeNode> _treeController;

  @override
  void initState() {
    super.initState();

    // To initalize _roots evalOnDart should be used here
    // after that, updating the roots or certain nodes can be done via events

    _treeController = TreeController<TreeNode>(
      roots: _roots,
      childrenProvider: (node) => node.children,
    );
    initEventStream();
  }

  Future<void> initEventStream() async {
    clientEventSub = serviceManager.service!.onExtensionEvent
        .where((event) => event.extensionKind?.startsWith('proto:') ?? false)
        .listen((event) {
      if (event.extensionKind == 'proto:stateUpdated') {
        if (event.extensionData?.data != null) {
          final data = event.extensionData!.data;
          final newRoots = [
            ...(data['roots'] as List).map((root) => TreeNode.fromJson(root))
          ];
          setState(() {
            _roots = newRoots;

            // the tree should probably updated with add/remove
            // to preserve 'expanded' state of nodes
            _treeController.roots = _roots;

            _rootCount = _roots.length;
          });
        }
      }
    });
  }

  @override
  void dispose() {
    clientEventSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RoundedOutlinedBorder(
      child: Column(
        children: [
          AreaPaneHeader(title: Text('Top level nodes: $_rootCount')),
          Expanded(
            child: Split(
              axis: Axis.horizontal,
              initialFractions: const [0.4, 0.6],
              minSizes: const [300.0, 300.0],
              children: [
                SizedBox(
                  width: 50,
                  child: TreeView<TreeNode>(
                    treeController: _treeController,
                    nodeBuilder:
                        (BuildContext context, TreeEntry<TreeNode> entry) {
                      return MyTreeTile(
                          entry: entry,
                          onTap: () {
                            if (entry.hasChildren) {
                              _treeController.toggleExpansion(entry.node);
                            }
                          });
                    },
                  ),
                ),
                const Center(
                  child: Text('Log of projected events will be here'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// Pasta from TreeView package docs with some edits
class MyTreeTile extends StatelessWidget {
  const MyTreeTile({
    super.key,
    required this.entry,
    required this.onTap,
  });

  final TreeEntry<TreeNode> entry;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      // Wrap your content in a TreeIndentation widget which will properly
      // indent your nodes (and paint guides, if required).
      //
      // If you don't want to display indent guides, you could replace this
      // TreeIndentation with a Padding widget, providing a padding of
      // `EdgeInsetsDirectional.only(start: TreeEntry.level * indentAmount)`
      child: TreeIndentation(
        entry: entry,
        // Provide an indent guide if desired. Indent guides can be used to
        // add decorations to the indentation of tree nodes.
        // This could also be provided through a DefaultTreeIndentGuide
        // inherited widget placed above the tree view.
        guide: const IndentGuide.connectingLines(indent: 48),
        // The widget to render next to the indentation. TreeIndentation
        // respects the text direction of `Directionality.maybeOf(context)`
        // and defaults to left-to-right.
        child: Padding(
          padding: const EdgeInsets.fromLTRB(4, 8, 8, 8),
          child: Row(
            children: [
              // Add a widget to indicate the expansion state of this node.
              // See also: ExpandIcon.
              if (entry.hasChildren)
                ExpandIcon(
                  isExpanded: entry.isExpanded,
                  onPressed: null,
                  // onPressed: entry.hasChildren ? (_) => onTap() : null,
                ),
              Text(entry.node.name),
            ],
          ),
        ),
      ),
    );
  }
}
